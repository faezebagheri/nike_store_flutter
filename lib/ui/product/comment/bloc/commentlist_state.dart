part of 'commentlist_bloc.dart';

abstract class CommentListState extends Equatable {
  const CommentListState();

  @override
  List<Object> get props => [];
}

class CommentlistLoading extends CommentListState {}

class CommentlistSuccess extends CommentListState {
  final List<CommentEntity> comments;

  const CommentlistSuccess(this.comments);

  @override
  List<Object> get props => [comments];
}

class CommentlistError extends CommentListState {
  final AppException exception;

  const CommentlistError(this.exception);
  
  @override
  List<Object> get props => [exception];
}

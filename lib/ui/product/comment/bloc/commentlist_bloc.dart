import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:nike/common/exceptions.dart';
import 'package:nike/data/comment.dart';
import 'package:nike/data/repo/comment_repository.dart';

part 'commentlist_event.dart';
part 'commentlist_state.dart';

class CommentListBloc extends Bloc<CommentlistEvent, CommentListState> {
  final CommentRepository repository;
  final int productId;
  CommentListBloc({required this.productId, required this.repository})
      : super(CommentlistLoading()) {
    on<CommentlistEvent>((event, emit) async {
      if (event is CommentListStarted) {
        emit(CommentlistLoading());
        try {
          final comments = await repository.getAll(productId: productId);
          emit(CommentlistSuccess(comments));
        } catch (e) {
          emit(CommentlistError(AppException()));
        }
      }
    });
  }
}

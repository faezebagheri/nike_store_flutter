import 'package:flutter/material.dart';
import 'package:nike/data/comment.dart';

class CommentItem extends StatelessWidget {
  final CommentEntity comment;
  const CommentItem({
    Key? key,
    required this.comment,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final themeData = Theme.of(context);
    return Container(
      padding: const EdgeInsets.all(12.0),
      margin: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 8.0),
      decoration: BoxDecoration(
        border: Border.all(
          color: themeData.dividerColor,
          width: 1.0,
        ),
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(comment.title),
                    const SizedBox(height: 4.0),
                    Text(
                      comment.email,
                      style: themeData.textTheme.caption,
                    ),
                  ],
                ),
              ),
              Text(
                comment.date,
                style: themeData.textTheme.caption,
              ),
            ],
          ),
          const SizedBox(height: 16.0),
          Text(
            comment.content,
            style: const TextStyle(
              height: 1.4,
            ),
          )
        ],
      ),
    );
  }
}

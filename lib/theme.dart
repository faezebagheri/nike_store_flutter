import 'dart:ui';

class LightThemeColors {
  static const primaryColor = Color(0xff218CF3);
  static const secondaryColor = Color(0xff262135);
  static const primaryTextColor = Color(0xff262135);
  static const secondaryTextColor = Color(0xffB3B6BE);
}
